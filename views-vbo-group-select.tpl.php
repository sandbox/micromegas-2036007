<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php 
  $vbo_button_title = explode(' ', $title);
  $vbo_css_class = 'vbo-group-select-' . substr(md5(rand()), 0, 7);
?>
<?php if (!empty($title)): ?>

  <div class="<?php print $vbo_css_class; ?> vbo-group-header">
  <h3><?php print $title; ?></h3><input class="<?php print $vbo_css_class; ?>" type="button"
  value="Select All <?php print $vbo_button_title[0]; ?>">
  </div>
  <div class="clearfix"> </div>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $vbo_css_class; ?> vbo-group-select-results">
    <?php print $row; ?>
  </div>
  <div class="clearfix"> </div>
<?php endforeach; ?>
