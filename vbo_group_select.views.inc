<?php

/**
 * Implementation of hook_views_plugins().
 */
function vbo_group_select_views_plugins() {
  return array(
    'style' => array(
      // Default settings for all style plugins.
      'vbo_group_select' => array(
        'title' => t('VBO group select'),
        'help' => t('Adds a button to select group members.'),
        'handler' => 'views_plugin_style_vbo_group_select',
        'theme' => 'views-vbo-group-select',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-vbo-group-select',
        'js' => 'vbo_group_select.js'
      ),
    ),
  );
}