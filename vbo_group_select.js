(function ($) {
  Drupal.behaviors.vboGroupSelect = {
  attach: function (context, settings) {
    jQuery('input[class^="vbo-group-select"]').click(function(){
        var vboGroup = $(this).attr("class");
        if (!this.checked) {
          $('.' + vboGroup + ' input').attr('checked', true);
        } else {
          $('.' + vboGroup + ' input').attr('checked', false);
        }
    });
  }};
})(jQuery);